import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router  from "./router";
import axios from 'axios';

Vue.config.productionTip = false;
// Vue.config.devtools = false;
axios.defaults.baseURL = 'http://www.omdbapi.com/?apikey=b80ca4ea&page=1&type=movie&Content-Type=application/json%27';
new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app');
